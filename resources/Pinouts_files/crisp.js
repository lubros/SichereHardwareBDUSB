$crisp.push(["on", "session:loaded", function() {
    if(document.referrer.indexOf("google.com") > -1) {
        $crisp.push(["set", "session:segments", [["google"]]]);
    }
    if(document.referrer.indexOf("gclid") > -1) {
        $crisp.push(["set", "session:segments", [["adwords"]]]);
    }
    if(document.referrer.indexOf("tbm=shop") > -1) {
        $crisp.push(["set", "session:segments", [["shopping"]]]);
    }
    if(document.referrer.indexOf("facebook.com") > -1) {
        $crisp.push(["set", "session:segments", [["facebook"]]]);
    }
}]);